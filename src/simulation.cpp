#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include "Robot.hpp"
#include "Cup.hpp"
#include <thread>
#include <fstream>
#include <sstream>

float rad(int deg)
{
  return deg / 180.0 * M_PI;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "simulation");

  std::ofstream myfile;

  ros::NodeHandle n;
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);

  Robot r;
  r.setup();

  ros::Subscriber sub = n.subscribe("robot_controller", 1000, &Robot::move_position, &r);

  tf::TransformBroadcaster broadcaster;
  ros::Rate loop_rate(1000);

  double angle = 0;

  // message declarations
  geometry_msgs::TransformStamped world;

  world.header.frame_id = "world";
  world.child_frame_id = "base_link";

  r.move_joint("upperarm2forearm", rad(-35), 2000, true);

  bool open = true;

  Cup c(0);

  std::thread t([&argv, &n]()
                {
                  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("robot_controller", 1000);
                  sleep(2);
                  if (strcmp(argv[1], "demo") == 0)
                  {
                    while (true)
                    {
                      std_msgs::String msg;
                      msg.data = "#1P1200T5000#2P1700T5000";
                      chatter_pub.publish(msg);
                      sleep(2);
                      msg.data = "#1P1200T1000#2P1700T1000";
                      chatter_pub.publish(msg);
                      sleep(1);
                      msg.data = "#5P1600T1000#6P1600T1000";
                      chatter_pub.publish(msg);
                      sleep(1);
                      msg.data = "#1P1500T5000#2P1500T5000";
                      chatter_pub.publish(msg);
                      sleep(2);
                      msg.data = "#5P1400T1000#6P1400T1000";
                      chatter_pub.publish(msg);
                      sleep(1);
                    }
                  }
                });

  bool started = false;
  ros::AsyncSpinner spinner(2);  // Use 4 threads
  spinner.start();

  while (ros::ok())
  {
    world.header.stamp = ros::Time::now();
    r.set_header_stamp(ros::Time::now());

    world.transform.translation.x = 0;
    world.transform.translation.y = 0;
    world.transform.translation.z = 0;
    world.transform.rotation = tf::createQuaternionMsgFromYaw(angle + M_PI / 2);

    if (!started)
    {
      c.init(0, 0.3, 1);
      started = true;
    }
    else
    {
      c.check_for_gripper(r);
    }

    c.publish(n, "visualization_marker");
    broadcaster.sendTransform(world);

    loop_rate.sleep();
  }

  t.join();
  return 0;
}
