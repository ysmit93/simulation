/*
 * Robot.cpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#include "Robot.hpp"
#include "Control_msg_Parser.hpp"
#include <chrono>
#include <fstream>
#include <vector>

class timer
{
private:
  unsigned long begTime;

public:
  void start()
  {
    begTime = clock();
  }

  double elapsedTime()
  {
    return (clock() - begTime) / (CLOCKS_PER_SEC / 1000);
  }

  bool isTimeout(unsigned long seconds)
  {
    return seconds >= elapsedTime();
  }
};

float map(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

Robot::Robot() : gripper_closed(false), right_gripper_closed(false), left_gripper_closed(false)
{
  // TODO Auto-generated constructor stub
  robot_pub = node.advertise<sensor_msgs::JointState>("joint_states", 1);
  std::thread t([this]()
                {
                  while (true)
                  {
                    tf::StampedTransform transform1;
                    tf::StampedTransform transform2;

                    try
                    {
                      if (listener.waitForTransform("/world", "/gripper_left", ros::Time(0), ros::Duration(5.0)))
                      {
                        listener.lookupTransform("/world", "/gripper_left", ros::Time(0), transform1);
                      }

                      if (listener.waitForTransform("/world", "/gripper_right", ros::Time(0), ros::Duration(5.0)))
                      {
                        listener.lookupTransform("/world", "/gripper_right", ros::Time(0), transform2);
                      }
                      run_marker(transform1, transform2);
                    }
                    catch (tf::TransformException &ex)
                    {
                      ROS_ERROR("%s", ex.what());
                    }
                  }
                });
  thread.swap(t);
}

Robot::~Robot()
{
  // TODO Auto-generated destructor stub
  thread.join();
}

void Robot::move_position(const std_msgs::String::ConstPtr &msg)
{
  Control_msg_Parser p;
  std::vector<std::tuple<int, int, int>> res = p.parse_control_command(msg->data.c_str());
  std::vector<std::thread> threads;

  threads.resize(res.size());

  for (unsigned int i = 0; i < res.size(); i++)
  {
    int pin = std::get<0>(res.at(i));
    int pos = std::get<1>(res.at(i));
    int time = std::get<2>(res.at(i));
    if ((pin == 6 || pin == 5) && pos > 1500)
    {
      std::thread t([this, pin, pos, time]()
                    {
                      move_joint(get_joint(pin), 0.007, time, true);
                    });
      threads[i].swap(t);
    }
    else if ((pin == 6 || pin == 5) && pos < 1500)
    {
      std::thread t([this, pin, pos, time]()
                    {
                      move_joint(get_joint(pin), 0.0, time, true);
                    });
      threads[i].swap(t);
    }
    else
    {
      std::thread t([this, pin, pos, time]()
                    {
                      move_joint(get_joint(pin), pos, time);
                    });
      threads[i].swap(t);
    }
  }

  for (unsigned int i = 0; i < threads.size(); i++)
  {
    threads[i].join();
  }
}

void Robot::move_joint(std::string joint, float position, float velocity, bool gripper /*= false*/)
{
  float step;
  if (!gripper)
  {
    position = map(position, 500, 2500, -90, 90);
    step = (to_rad(position) - joint_state.position[get_joint(joint)]) / velocity;
  }
  else
  {
    step = (position - joint_state.position[get_joint(joint)]) / velocity;
  }
  timer t1;
  timer t2;
  t1.start();
  t2.start();
  while (t1.elapsedTime() <= velocity)
  {
    if (t2.elapsedTime() >= 1)
    {
      t2.start();
      set_header_stamp(ros::Time::now());
      set_joint_position(joint, joint_state.position[get_joint(joint)] + step);
      publish_js("joint_states");
    }
  }
  if (get_joint(joint) == 5 && position > 0)
  {
    left_gripper_closed = true;
  }
  else if (get_joint(joint) == 6 && position > 0)
  {
    right_gripper_closed = true;
  }

  if (get_joint(joint) == 5 && position == 0)
  {
    left_gripper_closed = false;
  }
  else if (get_joint(joint) == 6 && position == 0)
  {
    right_gripper_closed = false;
  }

  if (left_gripper_closed && right_gripper_closed)
  {
    gripper_closed = true;
  }
  else
  {
    gripper_closed = false;
  }
}

void Robot::setup()
{
  joint_state.name.resize(7);
  joint_state.position.resize(7);
  joint_state.velocity.resize(7);
  // Determine the names of eacht joint.
  joint_state.name[0] = "base_link2turret";
  joint_state.name[1] = "turret2upperarm";
  joint_state.name[2] = "upperarm2forearm";
  joint_state.name[3] = "forearm2wrist";
  joint_state.name[4] = "wrist2hand";
  joint_state.name[5] = "gripper_left2hand";
  joint_state.name[6] = "gripper_right2hand";
}

void Robot::set_joint_position(std::string joint, float position)
{
  joint_state.position[get_joint(joint)] = position;
}

void Robot::close_gripper()
{
  std::thread t1([this]()
                 {
                   move_joint("gripper_left2hand", 0.007, 600, true);
                 });
  std::thread t2([this]()
                 {
                   move_joint("gripper_right2hand", 0.007, 600, true);
                 });
  t2.join();
  t1.join();
  gripper_closed = true;
}
void Robot::open_gripper()
{
  std::thread t1([this]()
                 {
                   move_joint("gripper_left2hand", 0.0, 200, true);
                 });
  std::thread t2([this]()
                 {
                   move_joint("gripper_right2hand", 0.0, 200, true);
                 });
  t2.join();
  t1.join();
  gripper_closed = false;
}

void Robot::set_header_stamp(ros::Time t)
{
  joint_state.header.stamp = t;
}

void Robot::publish_js(std::string topic)
{
  robot_pub.publish(joint_state);
}

unsigned int Robot::get_joint(std::string joint)
{
  for (unsigned int i = 0; i < joint_state.name.size(); i++)
  {
    if (joint_state.name[i] == joint)
    {
      return i;
    }
  }
}

std::string Robot::get_joint(const int pin)
{
  return joint_state.name[pin];
}

bool Robot::is_gripper_closed() const
{
  return gripper_closed;
}

void Robot::run_marker(tf::StampedTransform t1, tf::StampedTransform t2)
{
  marker.header.frame_id = "world";
  marker.header.stamp = ros::Time::now();

  marker.ns = "basic_shapes";
  marker.id = 1;

  marker.type = visualization_msgs::Marker::CUBE;

  marker.action = visualization_msgs::Marker::ADD;

  marker.pose.position.x = (t1.getOrigin().x() + t2.getOrigin().x()) / 2;
  marker.pose.position.y = (t1.getOrigin().y() + t2.getOrigin().y()) / 1.9;
  marker.pose.position.z = (t1.getOrigin().z() + t2.getOrigin().z()) / 2;

  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;

  marker.scale.x = 0.02;
  marker.scale.y = 0.02;
  marker.scale.z = 0.04;

  marker.color.r = 1.0f;
  marker.color.g = 1.0f;
  marker.color.b = 1.0f;
  marker.color.a = 0.0;

  marker.lifetime = ros::Duration();

  ros::Publisher marker_pub = node.advertise<visualization_msgs::Marker>("visualization_marker", 1);
  marker_pub.publish(marker);
}

sensor_msgs::JointState Robot::get_js() const
{
  return joint_state;
}
visualization_msgs::Marker Robot::get_marker() const
{
  return marker;
}

float Robot::to_rad(int deg)
{
  return deg / 180.0 * M_PI;
}
