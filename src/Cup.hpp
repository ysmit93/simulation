/*
 * Cup.hpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#ifndef SRC_CUP_HPP_
#define SRC_CUP_HPP_

#include <visualization_msgs/Marker.h>
#include "Robot.hpp"
#include <ros/ros.h>
#include <string>

class Cup
{
public:
  /**
   * Constructor with only id as parameter. The cup is placed in a random
   * location.
   * The location for xyz is between 0 and 1.
   */
  Cup(unsigned int id);

  /**
   * Constructor with coordinates.
   */
  Cup(unsigned int id, double x, double y, double z);

  /**
   * Destructor.
   */
  ~Cup();

  /**
   * Returns if the cup is grabbed.
   */
  bool isgrabbed() const;

  /**
   * Sets if the cup is grabbed.
   */
  void set_grabbed(bool grabbed);

  /**
   * Returns the id.
   */
  unsigned int get_id() const;

  /**
   * Checks if the gripper is around the cup.
   * @param r The robot to check the gripper from.
   */
  void check_for_gripper(const Robot &r);

  /**
   * Sets a random position for the Cup.
   */
  void init();

  /**
   * Sets the position of the cup.
   * @param x The x position of the cup.
   * @param y The y position of the cup.
   * @param z The z position of the cup.
   */
  void init(double x, double y, double z);

  /**
   * Publishes to a topic.
   * @param n The node to publish with.
   * @param The topic to publis on.
   */
  void publish(ros::NodeHandle n, std::string topic);

  /**
   * Moves the cup to a position.
   */
  void move_to_pos(int x, int y, int z);

  /**
   * Makes the marker go down when he isnt grabbed and he is above ground.
   */
  void gravity();

private:
  unsigned int id;
  bool grabbed;
  ros::Publisher marker_pub;
  visualization_msgs::Marker marker;
};

#endif /* SRC_CUP_HPP_ */
