#include "ros/ros.h"
#include "std_msgs/String.h"

#include <iostream>
#include <string>
#include <sstream>
#include <boost/regex.hpp>

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  try
  {
	  ros::init(argc, argv, "robot_controller_client");
	  ros::NodeHandle n;
	  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("robot_controller", 1000);
	  ros::Rate loop_rate(10);

	  while (ros::ok())
	  {
		std_msgs::String msg;
		std::string currentline;

		boost::regex exit(R"(^\s*((exit)+)\s*$)");
		std::getline(std::cin, currentline);

		boost::smatch result;
		if (boost::regex_match(currentline, result, exit))
		{
		  throw std::runtime_error("Exiting application!");
		}

		msg.data = currentline;

		chatter_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
	  }
  }
  catch (std::exception& e)
  {

  }

  return 0;
}
