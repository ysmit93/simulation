/*
 * Cup.cpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#include "Cup.hpp"
#include <cstdlib>

Cup::Cup(unsigned int id, double x, double y, double z) : id(id), grabbed(false)
{
  init(x, y, z);
}

Cup::Cup(unsigned int id) : id(id)
{
  float x = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  float y = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  float z = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  grabbed = false;
  init(x, y, z);
}

Cup::~Cup()
{
  // TODO Auto-generated destructor stub
}

bool Cup::isgrabbed() const
{
  return grabbed;
}

void Cup::set_grabbed(bool grabbed)
{
  this->grabbed = grabbed;
}

unsigned int Cup::get_id() const
{
  return id;
}

void Cup::init()
{
  float x = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  float y = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  float z = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  init(x, y, z);
}

void Cup::init(double x, double y, double z)
{
  marker.header.frame_id = "world";
  marker.header.stamp = ros::Time::now();

  marker.ns = "basic_shapes";
  marker.id = id;

  marker.type = visualization_msgs::Marker::CYLINDER;

  marker.action = visualization_msgs::Marker::ADD;

  marker.pose.position.x = x;
  marker.pose.position.y = y;
  marker.pose.position.z = z;

  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = -0.025;
  marker.pose.orientation.w = 1.0;

  marker.scale.x = 0.02;
  marker.scale.y = 0.02;
  marker.scale.z = 0.04;

  marker.color.r = 0.0f;
  marker.color.g = 1.0f;
  marker.color.b = 0.0f;
  marker.color.a = 1.0;
}

void Cup::move_to_pos(int x, int y, int z)
{
  marker.pose.position.x = x;
  marker.pose.position.y = y;
  marker.pose.position.z = z;
}

void Cup::publish(ros::NodeHandle n, std::string topic)
{
  marker_pub = n.advertise<visualization_msgs::Marker>(topic, 1);
  marker.lifetime = ros::Duration();
  marker_pub.publish(marker);
}

void Cup::check_for_gripper(const Robot &r)
{
  double marge = 0.01;
  if (r.is_gripper_closed() && marker.pose.position.x >= r.get_marker().pose.position.x - marge &&
      marker.pose.position.x <= r.get_marker().pose.position.x + marge &&
      marker.pose.position.y >= r.get_marker().pose.position.y - marge &&
      marker.pose.position.y <= r.get_marker().pose.position.y + marge &&
      marker.pose.position.z >= r.get_marker().pose.position.z - marge &&
      marker.pose.position.z <= r.get_marker().pose.position.z + marge)
  {
    set_grabbed(true);
    marker.pose.position.x = r.get_marker().pose.position.x;
    marker.pose.position.y = r.get_marker().pose.position.y;
    marker.pose.position.z = r.get_marker().pose.position.z;
  }

  else if (r.is_gripper_closed() == false && marker.pose.position.z > 0.02)
  {
    gravity();
  }
}

void Cup::gravity()
{
  marker.pose.position.z = marker.pose.position.z - 0.001;
}
