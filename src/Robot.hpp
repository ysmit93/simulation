/*
 * Robot.hpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#ifndef SRC_ROBOT_HPP_
#define SRC_ROBOT_HPP_

#include <sensor_msgs/JointState.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <ros/ros.h>
#include <thread>
#include <string>
#include "std_msgs/String.h"

class Robot
{
public:
  Robot();
  ~Robot();

  /**
   * Moves the robot
   * @param pos A position string of the format of the SSC-32U controller.
   */
  void move_position(const std_msgs::String::ConstPtr &msg);

  /**
   * Moves a single joint to a position
   * @param joint The name of the joint to move.
   * @param position
   */
  void move_joint(std::string joint, float position, float velocity, bool gripper = false);

  /**
   * Sets the joints and the range of motion of the robot.
   */
  void setup();

  /**
   * Closes the gripper of the Robot.
   *
   */
  void close_gripper();

  /**
   * Opens the gripper of the robot.
   */
  void open_gripper();

  /**
   * Sets the joint state header stamp.
   * @param t The time to set the stamp to.
   */
  void set_header_stamp(ros::Time t);

  /**
   * Returns if the gripper is closed.
   */
  bool is_gripper_closed() const;

  /**
   * Returns the joint states of the robot.
   */
  sensor_msgs::JointState get_js() const;

  /**
   * Returns the marker between the left and the right gripper.
   */
  visualization_msgs::Marker get_marker() const;

private:
  void set_joint_position(std::string joint, float position);
  void run_marker(tf::StampedTransform t1, tf::StampedTransform t2);
  void publish_js(std::string topic);
  bool right_gripper_closed;
  bool left_gripper_closed;
  bool gripper_closed;
  std::string get_joint(const int pin);
  unsigned int get_joint(std::string joint);
  float to_rad(int deg);

  tf::TransformListener listener;
  sensor_msgs::JointState joint_state;
  visualization_msgs::Marker marker;
  ros::Publisher robot_pub;
  ros::NodeHandle node;
  std::thread thread;
};

#endif /* SRC_ROBOT_HPP_ */
