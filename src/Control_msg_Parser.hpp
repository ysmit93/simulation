/*
 * Control_msg_Parser.hpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#ifndef SRC_CONTROL_MSG_PARSER_HPP_
#define SRC_CONTROL_MSG_PARSER_HPP_

#include "Robot.hpp"
#include <vector>
#include <string>
#include <tuple>

class Control_msg_Parser
{
public:
  /**
   * Default constructor
   */
  Control_msg_Parser();
  /**
   * Destructor
   */
  ~Control_msg_Parser();
  /**
   * Parser input in the format of #1P1T1 where the first digit is the pin/servo
   *number,
   * the second the position of the servo and the third the time.
   *
   * @param command String to be parsed.
   *
   * @return std::vector<std::tuple<int, int, int> > Tuple containing the parsed
   *values of the string.
   */
  std::vector<std::tuple<int, int, int>> parse_control_command(std::string command);
};

#endif /* SRC_CONTROL_MSG_PARSER_HPP_ */
