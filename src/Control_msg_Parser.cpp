/*
 * Control_msg_Parser.cpp
 *
 *  Created on: Oct 18, 2016
 *      Author: yoeri
 */

#include "Control_msg_Parser.hpp"
#include <boost/lexical_cast.hpp>
#include <stdexcept>
#include <boost/algorithm/string.hpp>

Control_msg_Parser::Control_msg_Parser()
{
  // TODO Auto-generated constructor stub
}

Control_msg_Parser::~Control_msg_Parser()
{
  // TODO Auto-generated destructor stub
}

// Helper function to determine if the character in the string is a digit.
inline bool is_digit(char c)
{
  return ('0' <= c && c <= '9');
}

std::vector<std::tuple<int, int, int>> Control_msg_Parser::parse_control_command(std::string command)
{
  int pin, position, time;
  std::vector<std::string> fields;

  std::string::iterator it;
  std::string string_pin, string_pos, string_time;

  boost::split(fields, command, boost::is_any_of("#"));
  std::vector<std::tuple<int, int, int>> values;

  bool hash = false;
  bool P = false;
  bool T = false;

  for (int i = 1; i < fields.size(); i++)
  {
    hash = true;
    for (it = fields.at(i).begin(); it != fields.at(i).end(); ++it)
    {
      if ((is_digit(*it)) && hash == true)
      {
        string_pin += (*it);
        pin = boost::lexical_cast<int>(string_pin);
      }
      if ((*it) == 'P')
      {
        hash = false;
        P = true;
        T = false;
      }
      if ((is_digit((*it))) && P == true)
      {
        string_pos += *it;
        position = boost::lexical_cast<int>(string_pos);
      }
      if ((*it) == 'T')
      {
        hash = false;
        P = false;
        T = true;
      }
      if ((is_digit((*it))) && T == true)
      {
        string_time += *it;
        time = boost::lexical_cast<int>(string_time);
      }
    }

    std::tuple<int, int, int> result;
    result = std::make_tuple(pin, position, time);
    values.push_back(result);

    hash = false;
    P = false;
    T = false;

    string_pin.clear();
    string_pos.clear();
    string_time.clear();
  }
  return values;
}
